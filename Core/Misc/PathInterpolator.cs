using UnityEngine;
using System.Collections.Generic;

namespace Pathfinding.Util {
	/// <summary>Interpolates along a sequence of points</summary>
	public class PathInterpolator {
		/// <summary>
		/// Represents a single point on the polyline represented by the \reflink{PathInterpolator}.
		/// The cursor is a lightweight structure which can be used to move backwards and forwards along a \reflink{PathInterpolator}.
		///
		/// If the \reflink{PathInterpolator} changes (e.g. has its path swapped out), then this cursor is invalidated and cannot be used anymore.
		/// </summary>
		public struct Cursor {
			private PathInterpolator interpolator;
			private int version;
			private float currentDistance;
			private float distanceToSegmentStart;
			private float currentSegmentLength;

			/// <summary>
			/// Current segment.
			/// The start and end points of the segment are path[value] and path[value+1].
			/// </summary>
			public int segmentIndex { get; internal set; }

			public int segmentCount {
				get {
					AssertValid();
					return interpolator.path.Count - 1;
				}
			}

			/// <summary>Last point in the path</summary>
			public Vector3 endPoint {
				get {
					AssertValid();
					return interpolator.path[interpolator.path.Count-1];
				}
			}

			/// <summary>A cursor at the start of the polyline represented by the interpolator</summary>
			public static Cursor StartOfPath (PathInterpolator interpolator) {
				if (!interpolator.valid) throw new System.InvalidOperationException("PathInterpolator has no path set");
				return new Cursor {
						   interpolator = interpolator,
						   version = interpolator.version,
						   segmentIndex = 0,
						   currentDistance = 0,
						   distanceToSegmentStart = 0,
						   currentSegmentLength = (interpolator.path[1] - interpolator.path[0]).magnitude,
				};
			}

			/// <summary>
			/// True if this instance has a path set.
			/// See: SetPath
			/// </summary>
			public bool valid {
				get {
					return interpolator != null && interpolator.version == version;
				}
			}

			/// <summary>Tangent of the curve at the current position</summary>
			public Vector3 tangent {
				get {
					AssertValid();
					return interpolator.path[segmentIndex+1] - interpolator.path[segmentIndex];
				}
			}

			/// <summary>Remaining distance until the end of the path</summary>
			public float remainingDistance {
				get {
					AssertValid();
					return interpolator.totalDistance - distance;
				}
				set {
					AssertValid();
					distance = interpolator.totalDistance - value;
				}
			}

			/// <summary>Traversed distance from the start of the path</summary>
			public float distance {
				get {
					return currentDistance;
				}
				set {
					AssertValid();
					currentDistance = value;

					while (currentDistance < distanceToSegmentStart && segmentIndex > 0) PrevSegment();
					while (currentDistance > distanceToSegmentStart + currentSegmentLength && segmentIndex < interpolator.path.Count - 2) NextSegment();
				}
			}

			/// <summary>Current position</summary>
			public Vector3 position {
				get {
					AssertValid();
					float t = currentSegmentLength > 0.0001f ? (currentDistance - distanceToSegmentStart) / currentSegmentLength : 0f;
					return Vector3.Lerp(interpolator.path[segmentIndex], interpolator.path[segmentIndex+1], t);
				}
			}

			/// <summary>Appends the remaining path between <see cref="position"/> and <see cref="endPoint"/> to buffer</summary>
			public void GetRemainingPath (List<Vector3> buffer) {
				AssertValid();
				buffer.Add(position);
				for (int i = segmentIndex+1; i < interpolator.path.Count; i++) {
					buffer.Add(interpolator.path[i]);
				}
			}

			void AssertValid () {
				if (!this.valid) throw new System.InvalidOperationException("The cursor has been invalidated because SetPath has been called on the interpolator. Please create a new cursor.");
			}

			/// <summary>Move to the specified segment and move a fraction of the way to the next segment</summary>
			public void MoveToSegment (int index, float fractionAlongSegment) {
				AssertValid();
				if (index < 0 || index >= interpolator.path.Count - 1) throw new System.ArgumentOutOfRangeException("index");
				while (segmentIndex > index) PrevSegment();
				while (segmentIndex < index) NextSegment();
				distance = distanceToSegmentStart + Mathf.Clamp01(fractionAlongSegment) * currentSegmentLength;
			}

			/// <summary>Move as close as possible to the specified point</summary>
			public void MoveToClosestPoint (Vector3 point) {
				AssertValid();

				float bestDist = float.PositiveInfinity;
				float bestFactor = 0f;
				int bestIndex = 0;

				var path = interpolator.path;

				for (int i = 0; i < path.Count-1; i++) {
					float factor = VectorMath.ClosestPointOnLineFactor(path[i], path[i+1], point);
					Vector3 closest = Vector3.Lerp(path[i], path[i+1], factor);
					float dist = (point - closest).sqrMagnitude;

					if (dist < bestDist) {
						bestDist = dist;
						bestFactor = factor;
						bestIndex = i;
					}
				}

				MoveToSegment(bestIndex, bestFactor);
			}

			public void MoveToLocallyClosestPoint (Vector3 point, bool allowForwards = true, bool allowBackwards = true) {
				AssertValid();

				var path = interpolator.path;

				segmentIndex = Mathf.Min(segmentIndex, path.Count - 2);
				while (true) {
					var factor = VectorMath.ClosestPointOnLineFactor(path[segmentIndex], path[segmentIndex+1], point);
					if (factor > 1.0f && allowForwards && segmentIndex < path.Count - 2) {
						NextSegment();
						allowBackwards = false;
					} else if (factor < 0.0f && allowBackwards && segmentIndex > 0) {
						PrevSegment();
						allowForwards = false;
					} else {
						if (factor > 0.5f && segmentIndex < path.Count - 2) {
							NextSegment();
						}
						break;
					}
				}

				// Check the distances to the two segments extending from the vertex path[segmentIndex]
				// and pick the position on those segments that is closest to the #point parameter.
				float factor1 = 0, factor2 = 0, d1 = float.PositiveInfinity, d2 = float.PositiveInfinity;

				if (segmentIndex > 0) {
					factor1 = VectorMath.ClosestPointOnLineFactor(path[segmentIndex-1], path[segmentIndex], point);
					d1 = (Vector3.Lerp(path[segmentIndex-1], path[segmentIndex], factor1) - point).sqrMagnitude;
				}

				if (segmentIndex < path.Count - 1) {
					factor2 = VectorMath.ClosestPointOnLineFactor(path[segmentIndex], path[segmentIndex+1], point);
					d2 = (Vector3.Lerp(path[segmentIndex], path[segmentIndex+1], factor2) - point).sqrMagnitude;
				}

				if (d1 < d2) MoveToSegment(segmentIndex - 1, factor1);
				else MoveToSegment(segmentIndex, factor2);
			}

			public void MoveToCircleIntersection2D<T>(Vector3 circleCenter3D, float radius, T transform) where T : IMovementPlane {
				AssertValid();

				var path = interpolator.path;

				// Move forwards as long as we are getting closer to circleCenter3D
				while (segmentIndex < path.Count - 2 && VectorMath.ClosestPointOnLineFactor(path[segmentIndex], path[segmentIndex+1], circleCenter3D) > 1) {
					NextSegment();
				}

				var circleCenter = transform.ToPlane(circleCenter3D);

				// Move forwards as long as the current segment endpoint is within the circle
				while (segmentIndex < path.Count - 2 && (transform.ToPlane(path[segmentIndex+1]) - circleCenter).sqrMagnitude <= radius*radius) {
					NextSegment();
				}

				// Calculate the intersection with the circle. This involves some math.
				var factor = VectorMath.LineCircleIntersectionFactor(circleCenter, transform.ToPlane(path[segmentIndex]), transform.ToPlane(path[segmentIndex+1]), radius);

				// Move to the intersection point
				MoveToSegment(segmentIndex, factor);
			}

			void PrevSegment () {
				segmentIndex--;
				currentSegmentLength = (interpolator.path[segmentIndex+1] - interpolator.path[segmentIndex]).magnitude;
				distanceToSegmentStart -= currentSegmentLength;
			}

			void NextSegment () {
				segmentIndex++;
				distanceToSegmentStart += currentSegmentLength;
				currentSegmentLength = (interpolator.path[segmentIndex+1] - interpolator.path[segmentIndex]).magnitude;
			}
		}

		List<Vector3> path;
		int version = 1;
		float totalDistance;

		/// <summary>
		/// True if this instance has a path set.
		/// See: SetPath
		/// </summary>
		public bool valid {
			get {
				return path != null;
			}
		}

		public Cursor start {
			get {
				return Cursor.StartOfPath(this);
			}
		}

		public Cursor AtDistanceFromStart (float distance) {
			var cursor = start;

			cursor.distance = distance;
			return cursor;
		}

		/// <summary>
		/// Set the path to interpolate along.
		/// This will invalidate all existing cursors.
		/// </summary>
		public void SetPath (List<Vector3> path) {
			this.version++;
			this.path = path;

			if (path == null) {
				totalDistance = float.PositiveInfinity;
				return;
			}

			if (path.Count < 2) throw new System.ArgumentException("Path must have a length of at least 2");

			var prev = path[0];

			totalDistance = 0;
			for (int i = 1; i < path.Count; i++) {
				var current = path[i];
				totalDistance += (current - prev).magnitude;
				prev = current;
			}
		}
	}
}
